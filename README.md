# CADock
**C**ontainerized **A**pplications with **Dock**er

Goal of this project is to provide an alternative to *Citrix XenApp* by building a container based "DockerApp" platform. Instead of virtualized appliances this project uses containerized appliances.

In the beginning I will only provide a Chrome app to start a Docker container remotely and execute an X passthrough in SSH. The next step is to run the Chrome app on *Chrome OS* as a thin client and use containerized applications remotely. Web app (**CADock Receiver**) and server part (**CADock DockerApp**) will be managed in two repositories and handled as submodules.

For starters this project only supports a single user and no session and authentication handling apart from ssh authentication. Multi user access and handling of seperated sessions will be added in the future.